def get_file_content():
    """function that prases file content for the server to use
    no params
    return:dictonary with albums as keys and songs as value, list of keys in the dictonary
    rtype: dictonary, list"""
    i =0
    data = {}
    lyrics = ""
    list_of_songs =[]
    album_key=()
    Path = "Pink_Floyd_DB.txt"
    file1 = open(Path,"r")
    file_content = file1.read()
    file1.close()
    file_content = file_content.split("#")
    file_content.remove('')    
    for album in file_content:
        album = album.split("*")
        i = 0
        for song in album:
            if i == 0:
                song = song.replace("\n","")
                album_key = tuple(song.split("::"))
                data[album_key] = []
            elif "::" in song:
                song = song.split("::")
                list_of_songs += [song]
            i += 1
        data[album_key] = list_of_songs
        list_of_songs = []
   
    key_list = list(data.keys())
    return data,key_list

