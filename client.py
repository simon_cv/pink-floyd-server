import socket

def build_msg(choice):
    """function that bulids a message according to the choice
    :param choice: the choice of the user
    :param type: int
    :return:client_msg
    :rtype:str"""
    if choice == 1:
        client_msg = "GET100"
    elif choice == 2:
        albumName = input("Enter name of album: ")
        client_msg = "GET200#{}".format(albumName)
    elif choice == 3:
        songName = input("Enter song name: ")
        client_msg = "GET300#{}".format(songName)
    elif choice == 4:
        songName = input("Enter song name: ")
        client_msg = "GET400#{}".format(songName)
    elif choice == 5:
        songName = input("Enter song name: ")
        client_msg = "GET500#{}".format(songName)
    elif choice == 6:
        word = input("Enter word: ")
        client_msg = "GET600#{}".format(word)
    elif choice == 7:
         lyric = input("Enter lyric: ")
         client_msg = "GET700#{}".format(lyric)
    elif choice == 8:
         client_msg = "GET800"
    return client_msg

def communicate_server(sock):
    """communicating with server
    :param sock: the socket with the server
    :param type: socket
    :return:none
    :rtype:none"""
    choice = 0
    loggedOrNot = ""
    server_resp = sock.recv(1024)
    print(server_resp.decode())
    sock.sendall(input().encode())
    loggedOrNot = sock.recv(1024).decode()
    if loggedOrNot == "Logged in!":
        print(loggedOrNot)
        print(sock.recv(1024).decode())
        while not choice == 8:     
            choice = int(input("Enter 1 for list of albums , 2 for list of songs in each album, 3 to get length of song, 4 to get lyrics of a song, 5 to find in which album the song is, 6 to search song by name, 7 to search song by lyrics and 8 to exit: "))
            while choice > 8 or choice < 1:
                print("Error! only 1-8")
                choice = int(input("Enter 1 for list of albums , 2 for list of songs in each album, 3 to get length of song, 4 to get lyrics of a song, 5 to find in which album the song is, 6 to search song by name, 7 to search song by lyrics and 8 to exit: "))
            client_msg = build_msg(choice)
            sock.sendall(client_msg.encode())
            server_resp = sock.recv(1024)
            print(server_resp.decode())
    else:
        print("Wrong password")
def main():  
    SERVER_IP = "127.0.0.1"
    SERVER_PORT = 69

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer 69
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    communicate_server(sock)
    sock.close()

if __name__ == "__main__":
    main()
