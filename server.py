import socket
import getData
import hashlib

def get_album_list(key_list):
    """function that gets the album list
    :param: key_list
    :type: list
    :return:resp
    rtype:str"""
    resp = "Albums are: "
    for i in key_list:
        resp += i[0] + ", "
    resp = resp[:-2]
    resp += ".\n"
    return resp

def get_song_len(key_list,song_to_find,data):
    """function that gets len of a song
    :param: key_list
    :type: list
    :param song_to_find: the song to find its len
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = ""
    found_or_not = 0
    for album in key_list:
        for song in data[album]:
            if song[0] == song_to_find:
                resp = "Lenght of song is " + song[2] + "."
                found_or_not = 1
    if not found_or_not:
        resp = "Song not found!"
    return resp

def get_song_lyrics(key_list,song_to_find,data):
    """function that gets lyrics of a song
    :param: key_list
    :type: list
    :param song_to_find: the song to find its lyrics
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = ""
    found_or_not = 0
    for album in key_list:
        for song in data[album]:
            if song[0] == song_to_find:
                resp = "Lyrics are: " + song[3]
                found_or_not = 1
    
    if not found_or_not:
        resp = "Song not found!"

    return resp


def get_songs_in_album(key_list,album_to_find,data):
    """function that gets the song in album
    :param: key_list
    :type: list
    :param album_to_find: the album to find
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = "Songs are: "
    found_or_not = 0
    for album in key_list:
        if album[0] == album_to_find:
            found_or_not = 1
            for song in data[album]:
                resp += song[0] +", "

    resp = resp[:-2]
    resp += "."
    if not found_or_not:
        resp = "Album not found!"
    return resp

def find_album(key_list,song_to_find,data):
    """function that finds in what album a song is
    :param: key_list
    :type: list
    :param song_to_find: the song to find its album
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = ""
    found_or_not = 0
    for album in key_list:
        for song in data[album]:
            if song[0] == song_to_find:
                resp = 'Song is in the album "' + album[0] + '".'
                found_or_not = 1
    if not found_or_not:
        resp = "Song not found!"
    return resp 

def find_song_by_word(key_list,word_to_find,data):
    """function that finds songs by word
    :param: key_list
    :type: list
    :param word_to_find: the word to find songs by it
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = "Songs are: "
    found_or_not = 0
    for album in key_list:
        for song in data[album]:
            if word_to_find.lower() in song[0].lower():
                resp += song[0] + ", "
                found_or_not = 1
    resp = resp[:-2]
    resp += "."
    if not found_or_not:
        resp = "Song not found!"

    return resp

def find_song_by_lyrics(key_list,lyrics_to_find,data):
    """function that find song by it lyrics
    :param: key_list
    :type: list
    :param lyrics_to_find: the lyrics to find songs by it
    :type: str
    :param data: dictonary with all of the data
    :type: dictonary
    :return:resp
    rtype:str"""
    resp = "Songs are: "
    found_or_not = 0
    for album in key_list:
        for song in data[album]:
            if lyrics_to_find.lower() in song[3].lower():
                resp += song[0] + ", "
                found_or_not = 1
    resp = resp[:-2]
    resp += "."
    if not found_or_not:
        resp = "Song not found!"
    return resp

def parse_client(client_sock):
    """giving client response and filtering the results
    :param client_sock: the socket with the client
    :param type: socket
    :return:none
    :rtype:none"""
    data,key_list = getData.get_file_content()
    
    client_msg = ""
    client_sock.sendall(b"Welcome")
    while True:
        client_msg = client_sock.recv(1024)
        client_msg = client_msg.decode()
        if "GET100" in client_msg:
            resp = get_album_list(key_list)
            client_sock.sendall(resp.encode())
        
        elif "GET200" in client_msg:
            resp = get_songs_in_album(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())
        elif "GET300" in client_msg:
            resp = get_song_len(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())
        
        elif "GET400" in client_msg:
            resp = get_song_lyrics(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())
        
        elif "GET500" in client_msg:
            resp = find_album(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())
        
        elif "GET600" in client_msg:
            resp = find_song_by_word(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())

        elif "GET700" in client_msg:
            resp = find_song_by_lyrics(key_list,client_msg[client_msg.find("#")+1:],data)
            client_sock.sendall(resp.encode())  
        
        elif "GET800" in client_msg:
            client_sock.sendall(b"Goodbye!")
            client_sock.close()
            break


def main():
    password = "952d2c56d0485958336747bcdd98590d"
    
    LISTEN_PORT = 69

    # Create a TCP/IP socket
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Binding to local port 69
    server_address = ('127.0.0.1', LISTEN_PORT)
    listening_sock.bind(server_address)

    # Listen for incoming connections
    listening_sock.listen(1)
    while True:
        # Create a new conversation socket
        client_soc, client_address = listening_sock.accept()
        client_soc.sendall(b"Enter password to continue:")
        if password == hashlib.md5(client_soc.recv(1024)).hexdigest():
            client_soc.sendall(b"Logged in!")
            parse_client(client_soc)
        else:
            client_soc.sendall(b"Wrong Password!")
            

if __name__ == "__main__":
    main()